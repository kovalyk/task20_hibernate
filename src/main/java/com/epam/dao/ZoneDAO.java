package com.epam.dao;

import com.epam.model.Zone;

public interface ZoneDAO extends GeneralDAO<Zone, Integer> {
}
