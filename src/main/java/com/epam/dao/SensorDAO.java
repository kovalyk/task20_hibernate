package com.epam.dao;

import com.epam.model.Sensor;

public interface SensorDAO extends GeneralDAO<Sensor, Integer> {
}
