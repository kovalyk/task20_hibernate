package com.epam.dao;

import com.epam.model.Room;

public interface RoomDAO extends GeneralDAO<Room, String> {
}
