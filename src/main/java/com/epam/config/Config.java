package com.epam.config;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Config {
    private static SessionFactory sessionFactory;

    private Config() {}

    public static org.hibernate.Session getSession() throws HibernateException {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            sessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
        return sessionFactory.openSession();
    }
}
