package com.epam.service;

import com.epam.dao.impl.RoomDaoImpl;
import com.epam.model.Room;

import java.sql.SQLException;
import java.util.List;

public class RoomService {

    public List<Room> findAll() throws SQLException {
        return new RoomDaoImpl().findAll();
    }

    public Room findById(String id) throws SQLException {
        return new RoomDaoImpl().findById(id);
    }

    public int create(Room entity) throws SQLException {
        return new RoomDaoImpl().create(entity);
    }

    public int update(Room entity) throws SQLException {
        return new RoomDaoImpl().update(entity);
    }

    public int delete(String id) throws SQLException {
        return new RoomDaoImpl().delete(id);
    }
}
