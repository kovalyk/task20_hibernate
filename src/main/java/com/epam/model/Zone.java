package com.epam.model;

import com.epam.annotation.Column;
import com.epam.annotation.Table;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "zone")
public class Zone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10)
    private int zoneId;
    @Column(name = "name", length = 45)
    private String zoneName;
    @OneToMany(mappedBy = "sensor")
    private List<Sensor> sensors;
    @OneToMany(mappedBy = "employee")
    private List<ZoneEmployee>zoneEmployees;

    public Zone() {
    }

    public Zone(int zoneId, String zoneName) {
        this.zoneId = zoneId;
        this.zoneName = zoneName;
    }

    public int getZoneId() {
        return zoneId;
    }

    public void setZoneId(int zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    public List<ZoneEmployee> getZoneEmployees() {
        return zoneEmployees;
    }

    public void setZoneEmployees(List<ZoneEmployee> zoneEmployees) {
        this.zoneEmployees = zoneEmployees;
    }

    @Override
    public String toString() {
        return "Zone{" +
                "zoneId=" + zoneId +
                ", zoneName='" + zoneName + '\'' +
                '}';
    }
}
