package com.epam.model;

import com.epam.annotation.Column;
import com.epam.annotation.Table;

import javax.persistence.*;

@Entity
@Table(name = "zone_employee")
public class ZoneEmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "zone_employee_id")
    private int zoneEmployeeId;
    @ManyToOne
    @JoinColumn(name = "zoneId")
    private Zone zone;
    @ManyToOne
    @JoinColumn(name = "employeeId")
    private Employee employee;



}
