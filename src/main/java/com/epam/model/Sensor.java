package com.epam.model;

import com.epam.annotation.Column;
import com.epam.annotation.Table;

import javax.persistence.*;

@Entity
@Table(name = "sensor")
public class Sensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10)
    private int sensorId;
    @Column(name = "type", length = 45)
    private String sensorType;
    @ManyToOne
    @JoinColumn(name = "zoneId")
    private Zone zone;

    public Sensor() {
    }

    public Sensor(int sensorId, String sensorType) {
        this.sensorId = sensorId;
        this.sensorType = sensorType;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "sensorId=" + sensorId +
                ", sensorType='" + sensorType + '\'' +
                '}';
    }
}
