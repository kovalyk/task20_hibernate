package com.epam.model;

import com.epam.annotation.Column;
import com.epam.annotation.Table;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10)
    private int empId;
    @Column(name = "first_name", length = 50)
    private String firstName;
    @Column(name = "second_name", length = 50)
    private String secondName;
    @Column(name = "job_status", length = 50)
    private String jobStatus;
    @OneToMany(mappedBy = "zone")
    private List<ZoneEmployee> zoneEmployees;

    public Employee() {
    }

    public Employee(int empId, String firstName, String secondName, String jobStatus) {
        this.empId = empId;
        this.firstName = firstName;
        this.secondName = secondName;
        this.jobStatus = jobStatus;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }


    @Override
    public String toString() {
        return "Employee{" +
                "empId=" + empId +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", jobStatus='" + jobStatus + '\'' +
                '}';
    }
}
